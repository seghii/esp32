 /* Hello World Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_spi_flash.h"
#include "freertos/semphr.h"

#include "var.h"
#include "mainFSM.h"
#include "UART.h"
#include "WiFisoft.h"


void app_main(void)
{
    InitUart ();
    InitWiFi ();
    InitVar  ();

    xTaskCreate(&UartRX_task,   "uart_rx_task",  1024*2, NULL, configMAX_PRIORITIES,   NULL);
    xTaskCreate(&UartTX_task,   "uart_tx_task",  1024*2, NULL, 3,   NULL);
    xTaskCreate(&run_fsm,       "fsm_task",      4096,   NULL, 5, NULL);
    xTaskCreate(&http_get_task, "http_get_task", 4096,   NULL, configMAX_PRIORITIES-1, NULL);

    
}
