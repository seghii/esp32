#ifndef WiFisoft_H
#define WiFisoft_H

/* Constants that aren't configurable in menuconfig */
#define WEB_SERVER "httpbin.org"
#define WEB_PORT 80
#define WEB_URL "http://httpbin.org/"

void InitWiFi (void);
void http_get_task(void *pvParameters);




#endif