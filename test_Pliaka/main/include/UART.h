#ifndef UART_H
#define UART_H


void InitUart (void);
void UartRX_task(void *arg);
void UartTX_task(void *arg);


#endif /*UART_H*/