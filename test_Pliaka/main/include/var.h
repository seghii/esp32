#ifndef var_H
#define var_H

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/semphr.h"

extern SemaphoreHandle_t xDoneWiFi;
extern SemaphoreHandle_t xRunWiFi;
extern SemaphoreHandle_t xRxUart  ;
extern SemaphoreHandle_t xTxUart  ;

extern uint8_t* pdata;
extern char *   precv_buf;
extern char *   REQUEST ;

extern char recv_buf[64];

void InitVar (void);



#endif 
