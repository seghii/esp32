#include <stdio.h>
#include "var.h"
#include "mainFSM.h"
#include "WiFisoft.h"



#define POST (1U)
#define GET  (2U)

typedef enum {idle=0,  activetWifi, waitWifi } emyFSMstate;
static emyFSMstate  state = idle;

static char* ERR_1 = "Unidentified request";


void run_fsm (void *arg)
{
  
    while(1){
        switch (state){
            case idle:
                if (xRxUart){ // received a message from the terminal
                    state = activetWifi;
                }
                break;
            case activetWifi:
                if (*(pdata) == GET){// we analyze the received message and form: 
                    REQUEST =   "GET "  WEB_URL  " HTTP/1.0\r\n"
                                "Host: "WEB_SERVER"\r\n"
                                "User-Agent: esp-idf/1.0 esp32\r\n"
                                "\r\n";
                    xSemaphoreGive(xRunWiFi);// semaphore the WiFi module
                    state = waitWifi;
                    break; 
                }
                else{
                    if(*(pdata) == POST){
                        REQUEST =   "POST "  WEB_URL  " HTTP/1.0\r\n"
                                    "Host: "WEB_SERVER"\r\n"
                                    "User-Agent: esp-idf/1.0 esp32\r\n"
                                    "LED_OFF" 
                                    "\r\n";
                        xSemaphoreGive(xRunWiFi);// semaphore the WiFi module
                        state = waitWifi;
                        break;
                    }       
                }
                    precv_buf =  ERR_1;
                xSemaphoreGive(xTxUart);        // run the transmitUart () task with error information
                break;
            case waitWifi:
                // The wait should be within 1 sec. if no response is received, we signal the terminal about the need to re-send the message
                if (xDoneWiFi){// waiting for a semaphore from the WiFi module
                    xSemaphoreGive(xTxUart);        // run the transmitUart () task
                }   
                state = idle;
                break;
            default:
                break;
        } 
    }
}


