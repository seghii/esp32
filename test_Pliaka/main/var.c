#include "var.h"
#include "WiFisoft.h"

SemaphoreHandle_t xDoneWiFi; 
SemaphoreHandle_t xRxUart  ;
SemaphoreHandle_t xRunWiFi ;
SemaphoreHandle_t xTxUart  ;
uint8_t* pdata;
char *   precv_buf;
char *   REQUEST;

char recv_buf[64];

void InitVar (void)
{
    xDoneWiFi = xSemaphoreCreateBinary();
    xRxUart   = xSemaphoreCreateBinary();
    xRunWiFi  = xSemaphoreCreateBinary();
    xTxUart   = xSemaphoreCreateBinary();
    pdata = NULL;
    precv_buf = recv_buf;

    REQUEST = "GET " WEB_URL " HTTP/1.0\r\n"
    "Host: "WEB_SERVER"\r\n"
    "User-Agent: esp-idf/1.0 esp32\r\n"
    "\r\n";

}
