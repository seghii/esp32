
#include <stdio.h>
#include "driver/uart.h"
#include "string.h"
#include "driver/gpio.h"

#include "var.h"
#include "UART.h"
#include "mainFSM.h"


static const int RX_BUF_SIZE = 1024;

#define TXD_PIN (GPIO_NUM_4)
#define RXD_PIN (GPIO_NUM_5)


void InitUart (void){
    const uart_config_t uart_config = {
        .baud_rate = 115200,
        .data_bits = UART_DATA_8_BITS,
        .parity = UART_PARITY_DISABLE,
        .stop_bits = UART_STOP_BITS_1,
        .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
        .source_clk = UART_SCLK_APB,
    };
    // We won't use a buffer for sending data.
    uart_driver_install(UART_NUM_1, RX_BUF_SIZE * 2, 0, 0, NULL, 0);
    uart_param_config(UART_NUM_1, &uart_config);
    uart_set_pin(UART_NUM_1, TXD_PIN, RXD_PIN, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    
}

int sendData(const char* logName, const char* data)
{
    const int len = strlen(data);
    const int txBytes = uart_write_bytes(UART_NUM_1, data, len);
//    ESP_LOGI(logName, "Wrote %d bytes", txBytes);
    return txBytes;
}

void UartTX_task(void *arg)
{
    const char *TX_TASK_TAG = "TX_TASK";
    while (1) {
        xSemaphoreTake(xTxUart, portMAX_DELAY);		     // The program here will fall into WAIT until the semaphore appears
        sendData(TX_TASK_TAG,   precv_buf);               // and when an event occurs, we send data to the terminal
    //    vTaskDelay(2000 / portTICK_PERIOD_MS);
    }
}

void UartRX_task(void *arg)
{
    uint8_t* data = (uint8_t*) malloc(RX_BUF_SIZE+1);
    while (1) {
        const int rxBytes = uart_read_bytes(UART_NUM_1, data, RX_BUF_SIZE, 1000 / portTICK_RATE_MS);
        if (rxBytes > 0) {
            data[rxBytes] = 0;
            pdata = data;
           xSemaphoreGive(xRxUart);	//  signaling the receipt of data from the terminal
        }
    }
    free(data); //I'm not sure what exactly to free memory here.
}